﻿using Archetype.Models;
using Examine;
using Newtonsoft.Json;
using System;
using Umbraco.Core;
using Umbraco.Core.Logging;
using System.Collections.Generic;

namespace umbraco_cms.Indexer
{

    public class ArchetypeExamine : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"].GatheringNodeData += ExamineMunger_GatheringNodeData;
        }

        private void ExamineMunger_GatheringNodeData(object sender, IndexingNodeDataEventArgs nodeData)
        {
            try
            {
                foreach(var field in nodeData.Fields)
                {
                    if (!field.Value.Contains("fieldset"))
                    {
                        continue;
                    }

                    _handlePageBuilderModules(nodeData, field);
                    nodeData.Fields.Remove(field.Key);
                }           
            }
            catch (Exception ex)
            {
                LogHelper.Info<ArchetypeExamine>(string.Format("Exception while munging nodeId: {0}", nodeData.NodeId));
                LogHelper.Error<ArchetypeExamine>(ex.Message, ex);
            }
        }

        private static void _handlePageBuilderModules(IndexingNodeDataEventArgs nodeData, KeyValuePair<string, string> field)
        {   
            try
            {               
                var modules = JsonConvert.DeserializeObject<ArchetypeModel>(field.Value);    
                _addMapToExamineFields(nodeData, modules);                
            }
            catch (Exception ex)
            {
                LogHelper.Info<ArchetypeExamine>(string.Format("Exception for current module type: {0}", field.Key));
                LogHelper.Error<ArchetypeExamine>(ex.Message, ex);
            }
        }

        private static void _addMapToExamineFields(IndexingNodeDataEventArgs nodeData, ArchetypeModel map, string prefix = "")
        {
            foreach (var item in map)
            {
                _addMapToExamineFields(nodeData, item, prefix);
            }
        }

        private static void _addMapToExamineFields(IndexingNodeDataEventArgs nodeData, ArchetypeFieldsetModel item, string prefix = "")
        {
            if (item == null)
            {
                return;
            }

            foreach (var property in item.Properties)
            {
                if (property.Value.ToString().Contains("fieldset"))
                {
                    _addMapToExamineFields(nodeData, JsonConvert.DeserializeObject<ArchetypeModel>(property.Value.ToString()), property.Alias + ".");
                }
                else
                {
                    nodeData.Fields.Add(prefix + property.Alias, property.Value.ToString());                    
                }
            }
        }        
    }
}
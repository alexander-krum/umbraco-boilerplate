﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Examine;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace umbraco_cms.Models
{
    public class KnowledgeBaseSearchResultModel
    {
        public string ID { get; set; }
        public string Url { get; set; }
        public string Summary { get; set; }
        public string PageName { get; set; }
        public object BreadCrumbs { get; set; }

        public KnowledgeBaseSearchResultModel(SearchResult searchResult, UmbracoHelper helper)
        {
            ID = searchResult.Fields.FirstOrDefault(s => s.Key.Equals("id")).Value;
            Url = helper.TypedContent(ID).Url;
            Summary = searchResult.Fields.FirstOrDefault(s => s.Key.Equals("summary")).Value;
            PageName = searchResult.Fields.FirstOrDefault(s => s.Key.Equals("nodeName")).Value;
            BreadCrumbs = helper.TypedContent(ID).AncestorsOrSelf().Select(s => new {Name=s.Name, Url = s.Url}).ToList().SkipLast();
        }
    }

}
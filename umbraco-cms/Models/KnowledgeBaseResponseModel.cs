﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace umbraco_cms.Models
{
    public class KnowledgeBaseResponseModel
    {
        public string Query { get; set; }
        public List<KnowledgeBaseSearchResultModel> Data { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace umbraco_cms.Models
{
    public class MultiNodePickerValueModel
    {
        public string Name { get; set; }
        public string Udi { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace umbraco_cms.Models
{
    public class KartelMapperConfigModel
    {
        public string Name { get; set; }    
        public List<string> Filters { get; set; }
    }
}
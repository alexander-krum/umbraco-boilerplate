﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;
using umbraco_cms.Helpers;
using Umbraco.Core.Models;
using Examine;
using Examine.SearchCriteria;
using Umbraco.Web;

namespace umbraco_cms.Controllers
{
    [Authorize]
    public class ContentController : UmbracoApiController
    {

        // GET: api/Content/5
        [AllowAnonymous]
        public async Task<IHttpActionResult> Get(string query, int limit=15, int skip=0)
        {                        
            var searcher = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
            var searchCreateria = searcher.CreateSearchCriteria(BooleanOperation.Or);
            try
            {
                var rawQuery = searchCreateria.RawQuery(query);
                var results = searcher.Search(rawQuery);

                if (results.TotalItemCount <= 0)
                {
                    return NotFound();
                }

                var contentList = await GetContentList(results);
                return Ok(ArchetypeMapper.Map(contentList, limit, skip));

            }
            catch (Exception e)
            {
                //TODO log error
                return InternalServerError(new Exception($"internal error: {e}"));
            }           
        }

        public async Task<IHttpActionResult> GetPage(string id)
        {
            var searcher = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
            var searchCreateria = searcher.CreateSearchCriteria(BooleanOperation.Or);
            try
            {
                var rawQuery = searchCreateria.RawQuery($"nodeName:{id}");
                var results = searcher.Search(rawQuery);

                if (results.TotalItemCount <= 0)
                {
                    return NotFound();
                }

                var contentList = await GetContentList(results);
                return Ok(ArchetypeMapper.Map(contentList.First()));
            }
            catch (Exception e)
            {
                //TODO log error
                return InternalServerError(new Exception($"internal error: {e}"));
            }
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> GetByNodeUrl(string url, int nesting = 0)
        {            
            var content = UmbracoContext.Current.ContentCache.GetByRoute(url);
            if (content == null)
            {
                return NotFound();
            }
            return Ok(ArchetypeMapper.MapByGroups(content, nesting));
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> GetRoot(string url)
        {
            var content = await System.Threading.Tasks.Task.Run(() => UmbracoContext.Current.ContentCache.GetByRoute(url));
            if (content == null)
            {
                return NotFound();
            }
            return Ok(ArchetypeMapper.Map(content));
        }

        private Task<IEnumerable<IPublishedContent>> GetContentList(ISearchResults results)
        {
            return System.Threading.Tasks.Task.Run(() => { return Umbraco.TypedContent(results.Select(x => x.Id)); });
        }
    }    
}

﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using umbraco_cms.Models.UmbracoIdentity;
using Umbraco.Web.WebApi;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;
using UmbracoIdentity;

namespace umbraco_cms.Controllers
{
    [System.Web.Mvc.Authorize]
    public class AccountController : UmbracoApiController
    {        
        private UmbracoMembersUserManager<UmbracoApplicationMember> _userManager;
        private UmbracoMembersRoleManager<UmbracoApplicationRole> _roleManager;

        public MediaService MediaService { get { return (MediaService)ApplicationContext.Services.MediaService; } }

        public AccountController(UmbracoContext umbracoContext, UmbracoMembersUserManager<UmbracoApplicationMember> userManager, UmbracoMembersRoleManager<UmbracoApplicationRole> roleManager) : base(umbracoContext)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public AccountController(UmbracoContext umbracoContext, UmbracoHelper umbracoHelper, UmbracoMembersUserManager<UmbracoApplicationMember> userManager, UmbracoMembersRoleManager<UmbracoApplicationRole> roleManager) : base(umbracoContext, umbracoHelper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public AccountController(UmbracoMembersUserManager<UmbracoApplicationMember> userManager, UmbracoMembersRoleManager<UmbracoApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public AccountController()
        {

        }

        protected IOwinContext OwinContext
        {
            get { return HttpContext.Current.GetOwinContext(); }
        }

        public UmbracoMembersUserManager<UmbracoApplicationMember> UserManager
        {
            get
            {
                return _userManager ?? (_userManager = OwinContext
                           .GetUserManager<UmbracoMembersUserManager<UmbracoApplicationMember>>());
            }
        }

        public UmbracoMembersRoleManager<UmbracoApplicationRole> RoleManager
        {
            get
            {
                return _roleManager ?? (_roleManager = OwinContext
                           .Get<UmbracoMembersRoleManager<UmbracoApplicationRole>>());
            }
        }

        // GET: api/Account
        public async Task<HttpResponseMessage> Get()
        {
            if (User == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,new UmbracoApplicationMember());
            }

            var userId = Int32.Parse(User.Identity.GetUserId());
            var member = await UserManager.FindByIdAsync(userId);

            if (member == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new UmbracoApplicationMember());
                
            }

            return Request.CreateResponse(HttpStatusCode.OK, member);
        }


        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        public async Task<HttpResponseMessage> Signup(RegisterModel model)
        {
            if (ModelState.IsValid == false)
            {
                return null;
            }

            var user = new UmbracoApplicationMember()
            {
                Name = model.Name,
                UserName = model.UsernameIsEmail || model.Username == null ? model.Email : model.Username,
                Email = model.Email,
                MemberProperties = model.MemberProperties,
                MemberTypeAlias = model.MemberTypeAlias
            };

            var result = await UserManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                //var newMedia = MediaService.CreateMedia(user.Id.ToString(), Root, "Folder", user.Id);
                //MediaService.Save(newMedia);

                await SignInAsync(user, isPersistent: false);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, user);
        }

        private async Task SignInAsync(UmbracoApplicationMember member, bool isPersistent)
        {
            OwinContext.Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            OwinContext.Authentication.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent },
                await member.GenerateUserIdentityAsync(UserManager));
        }
    }
}
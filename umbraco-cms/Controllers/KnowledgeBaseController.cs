﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Examine;
using Examine.SearchCriteria;
using umbraco_cms.Models;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace umbraco_cms.Controllers
{
    public class KnowledgeBaseController : UmbracoApiController
    {
        // GET: api/Content/5
        [AllowAnonymous]
        public async Task<IHttpActionResult> Get(string query, int limit = 5)
        {
            var searcher = ExamineManager.Instance.SearchProviderCollection["KnowledgeBase"];
            var searchCreateria = searcher.CreateSearchCriteria(BooleanOperation.Or);
            try
            {               
                var rawQuery = searchCreateria.RawQuery(query);
                var results = searcher.Search(rawQuery,limit);
                var response = new KnowledgeBaseResponseModel
                {
                    Query = query,
                    Data = results.Select(s => new KnowledgeBaseSearchResultModel(s, Umbraco)).ToList()
                }; 
                return Ok(response);
            }
            catch (Exception e)
            {
                //TODO log error
                return InternalServerError(new Exception($"internal error: {e}"));
            }
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> GetAll(string query)
        {
            var searcher = ExamineManager.Instance.SearchProviderCollection["KnowledgeBase"];
            var searchCreateria = searcher.CreateSearchCriteria(BooleanOperation.Or);
            try
            {
                var rawQuery = searchCreateria.RawQuery(query);
                var results = searcher.Search(rawQuery);
                var response = new KnowledgeBaseResponseModel
                {
                    Query = query,
                    Data = results.Select(s => new KnowledgeBaseSearchResultModel(s, Umbraco)).ToList()
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                //TODO log error
                return InternalServerError(new Exception($"internal error: {e}"));
            }
        }

        private Task<IEnumerable<IPublishedContent>> GetContentList(ISearchResults results)
        {
            return System.Threading.Tasks.Task.Run(() => { return Umbraco.TypedContent(results.Select(x => x.Id)); });
        }
    }
}

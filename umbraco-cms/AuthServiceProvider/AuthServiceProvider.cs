﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Cors;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using umbraco_cms.Models.UmbracoIdentity;
using Umbraco.IdentityExtensions;
using UmbracoIdentity;

namespace umbraco_cms.AuthServiceProvider
{
    public class MemberAuthServiceprovider : OAuthAuthorizationServerProvider
    {
        private readonly BackOfficeAuthServerProviderOptions _options;

        public MemberAuthServiceprovider(BackOfficeAuthServerProviderOptions options = null)
        {
            if (options == null)
                options = new BackOfficeAuthServerProviderOptions();
            _options = options;
        }

        public override Task ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        {
            ProcessCors(context);

            return base.ValidateTokenRequest(context);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<UmbracoMembersUserManager<UmbracoApplicationMember>>();
            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await userManager.ClaimsIdentityFactory.CreateAsync(userManager, user, OAuthDefaults.AuthenticationType);

            context.Validated(oAuthIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string id)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "id", id}
            };
            return new AuthenticationProperties(data);
        }

        private void ProcessCors(OAuthValidateTokenRequestContext context)
        {
            var accessControlRequestMethodHeaders = context.Request.Headers.GetCommaSeparatedValues(CorsConstants.AccessControlRequestMethod);
            var originHeaders = context.Request.Headers.GetCommaSeparatedValues(CorsConstants.Origin);
            var accessControlRequestHeaders = context.Request.Headers.GetCommaSeparatedValues(CorsConstants.AccessControlRequestMethod);
            var corsRequest = new CorsRequestContext
            {
                Host = context.Request.Host.Value,
                HttpMethod = context.Request.Method,
                Origin = originHeaders == null ? null : originHeaders.FirstOrDefault(),
                RequestUri = context.Request.Uri,
                AccessControlRequestMethod = accessControlRequestMethodHeaders == null ? null : accessControlRequestMethodHeaders.FirstOrDefault(),

            };
            if (accessControlRequestHeaders != null)
            {
                foreach (var header in context.Request.Headers.GetCommaSeparatedValues(CorsConstants.AccessControlRequestMethod))
                {
                    corsRequest.AccessControlRequestHeaders.Add(header);
                }
            }

            var engine = new CorsEngine();

            if (corsRequest.IsPreflight)
            {
                try
                {
                    // ReSharper disable once UnusedVariable
                    var test = new HttpMethod(corsRequest.AccessControlRequestMethod);
                }
                catch (ArgumentException)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    context.SetError("Access Control Request Method Cannot Be Null Or Empty");
                    return;
                }
                catch (FormatException)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    context.SetError("Invalid Access Control Request Method");
                    return;
                }

                var result = engine.EvaluatePolicy(corsRequest, _options.CorsPolicy);

                if (!result.IsValid)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    context.SetError(string.Join(" | ", result.ErrorMessages));
                    return;
                }

                WriteCorsHeaders(result, context);
            }
            else
            {
                var result = engine.EvaluatePolicy(corsRequest, _options.CorsPolicy);


                if (result.IsValid)
                {
                    WriteCorsHeaders(result, context);
                }
            }
        }

        private void WriteCorsHeaders(CorsResult result, OAuthValidateTokenRequestContext context)
        {
            var headers = result.ToResponseHeaders();

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    context.Response.Headers.Append(header.Key, header.Value);
                }
            }
        }
    }
}
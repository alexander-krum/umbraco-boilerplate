﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using Archetype.Models;
using Newtonsoft.Json;
using ServiceStack.Configuration;
using umbraco;
using umbraco_cms.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace umbraco_cms.Helpers
{
    public class ArchetypeMapper
    {     
        /// <summary>
        /// This Function is used by the Get Controller, since it has 
        /// </summary>
        /// <param name="results">Content Nodes found by examine</param>
        /// <param name="limit">Limit for the results</param>
        /// <param name="skip">The number of the results skipped</param>
        /// <returns></returns>
        public static Dictionary<string, object> Map(IEnumerable<IPublishedContent> results, int limit, int skip)
        {               
            var data = new List<object>();
            if (skip < results.Count())
            {
                foreach (var result in results.Skip(skip).Take(limit))
                {
                    data.Add(MapByGroups(result));
                }
            }
            
            return new Dictionary<string, object>
            {
                {"data", data},
                {"count", results.Count()},
                {"more", results.Count() > (skip + limit)}               
            };                                              
        }
        
        /// <summary>
        /// Similar function as MapByGroups just without grouping
        /// </summary>
        /// <param name="content">content node</param>
        /// <returns>Content properties mapped to dictionary</returns>
        public static Dictionary<string, object> Map(IPublishedContent content)
        {
           var list = new Dictionary<string, object>();                    
           foreach (var property in content.Properties)
            {                
                if (IsArchetype(property))
                {
                    var model = content.GetPropertyValue<ArchetypeModel>(property.PropertyTypeAlias);
                    list.Add(GetPropertyName(content, property), MapDictionary(model));
                }
                else if (property.Value.ToString().Contains("RJP"))
                {
                    var data = convertToObject(property.DataValue.ToString());
                    list.Add(GetPropertyName(content, property), data);
                }                
                else
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    if (property.DataValue.ToString().Contains("umb") && property.DataValue.ToString().Contains("media"))
                    {
                        var url = Udi.Parse(property.DataValue.ToString());                        
                        var value = helper.TypedMedia(url);
                        list.Add(GetPropertyName(content, property), value.Url);
                    }
                    else if (property.DataValue.ToString().Contains("umb"))
                    {
                        var url = Udi.Parse(property.DataValue.ToString());
                        var value = helper.TypedContent(url);
                        list.Add(GetPropertyName(content, property), value.Url);
                    }
                    else
                    {
                        list.Add(GetPropertyName(content, property), property.DataValue);
                    }                    
                }
            }
            
            return list;
        }

        /// <summary>
        /// Method is used to convert the json string data value from Multiple 
        /// Url Picker
        /// </summary>
        /// <param name="propertyDataValue">The data string</param>
        /// <returns></returns>
        private static object convertToObject(string propertyDataValue)
        {
            
            var dict = new Dictionary<string, string>();
            var data = Json.Decode(propertyDataValue);
            if (data == null)
            {
                return dict;
            }

            var helper = new UmbracoHelper(UmbracoContext.Current);
            foreach (var item in data)
            {
                try
                {
                    var isPublishedContent = item.udi != null;                   
                    if (isPublishedContent && item.udi.ToString().Contains("media"))
                    {
                        var url = Udi.Parse(item.udi);
                        var value = helper.TypedMedia(url);
                        dict.Add(item.name.ToString(), value != null ? value.Url : string.Empty);
                    }
                    else if (isPublishedContent)
                    {
                        var url = Udi.Parse(item.udi);
                        var value = helper.TypedContent(url);
                        dict.Add(item.name.ToString(), value != null ? value.Url : string.Empty);
                    }
                    else
                    {                        
                        dict.Add(item.name.ToString(), string.IsNullOrEmpty(item.url) ? "undefined" : item.url.ToString() );
                    }
                }
                catch (Exception e)
                {                    
                    continue;
                }                
            }

            return dict;
        }

        /// <summary>
        /// Method used to return an content with properties grouped by Virtual Tabs in Umbraco
        /// </summary>
        /// <param name="content">Content node</param>
        /// <returns>Dictionary with Key Value pairs</returns>
        public static Dictionary<string, object> MapByGroups(IPublishedContent content, int numberOfNestedLevels = 0, bool withContent=true)
        {
            var canNest = Boolean.Parse(ConfigurationManager.AppSettings["nestingConfigurable"]);
            if (!canNest)
            {
                numberOfNestedLevels = Int32.Parse(ConfigurationManager.AppSettings["defaultNesting"]);
            }
           
            var list = new Dictionary<string, object>();
                          
            list.Add("pageName", content.Name);
            list.Add("pageType", content.DocumentTypeAlias);
            list.Add("id", content.Id);
            list.Add("url", content.Url);
            list.Add("BreadCrumbs", content.AncestorsOrSelf().Select(s => new { Name = s.Name, Url = s.Url }).ToList().SkipLast());

            var groups = GetTabs(content);
            var ungroupedElems = GetGroups();
            foreach (var group in groups)
            {
                var map = Map(content, group);

                if (ungroupedElems.Contains(group.Name))
                {
                    foreach (var property in map)
                    {
                        if (list.ContainsKey(ToJsonPropName(property.Key)))
                        {
                            continue;
                        }
                        list.Add(property.Key, property.Value);
                    }
                }
                else if (list.ContainsKey(ToJsonPropName(group.Name)))
                {
                    list.TryGetValue(ToJsonPropName(group.Name), out var value);
                    if (!(value is Dictionary<string, object> valueDict))
                    {
                        throw new InvalidCastException();
                    }

                    foreach (var property in map)
                    {
                        //Some key contains same content, therefore skip
                        if (valueDict.ContainsKey(ToJsonPropName(property.Key)))
                        {
                            continue;
                        }
                        valueDict.Add(property.Key, property.Value);
                    }
                }
                else
                {
                    list.Add(ToJsonPropName(group.Name), map);
                }               
            }
            
            if (numberOfNestedLevels > 0)
            {
                var children = GetChildrenList(content.Children, numberOfNestedLevels);
                list.Add("children", children);
            }
            
            return list;
        }

        /// <summary>
        /// The method maps all properties in a group
        /// </summary>
        /// <param name="content">Content node</param>
        /// <param name="group">Group from the content argument</param>
        /// <returns>Group properties as Key Value pairs</returns>
        private static Dictionary<string, object> Map(IPublishedContent content, PropertyGroup group)
        {
            var list = new Dictionary<string, object>();

            foreach (var propertyType in group.PropertyTypes)
            {
                var property = content.Properties
                    .FirstOrDefault(x => GetPropertyName(content, x)
                        .Equals(ToJsonPropName(propertyType.Name)));

                if (property == null)
                {
                    return new Dictionary<string, object>();
                }

                if (IsArchetype(property))
                {
                    var model = content.GetPropertyValue<ArchetypeModel>(property.PropertyTypeAlias);
                    
                    list.Add(GetPropertyName(content, property), MapDictionary(model));
                }
                else
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    if (property.DataValue.ToString().Contains("umb") && property.DataValue.ToString().Contains("media"))
                    {
                        var url = Udi.Parse(property.DataValue.ToString());
                        var value = helper.TypedMedia(url);
                        list.Add(GetPropertyName(content, property), value.Url);
                    }
                    else if (property.DataValue.ToString().Contains("umb"))
                    {
                        if (property.DataValue.ToString().Contains("name") &&
                            property.DataValue.ToString().Contains("udi"))
                        {
                            var linkList = new List<object>();
                            foreach (var link in Json.Decode<List<MultiNodePickerValueModel>>(property.DataValue.ToString()))
                            {
                                var url = Udi.Parse(link.Udi);
                                var value = helper.TypedContent(url);
                                linkList.Add(new {name=link.Name, url = value.Url ?? ""});
                            }
                               
                            list.Add(GetPropertyName(content, property), linkList);
                        }
                        else
                        {
                            var url = Udi.Parse(property.DataValue.ToString());
                            var value = helper.TypedContent(url);
                            list.Add(GetPropertyName(content, property), value.Url);
                        }                        
                    }
                    else
                    {
                        list.Add(GetPropertyName(content, property), property.DataValue);
                    }                    
                }
            }
            
            return list;
        }

        /// <summary>
        /// Checks whether property is of type Archetype
        /// </summary>
        /// <param name="property">Property</param>
        /// <returns>true if the type is archetype else false</returns>
        private static bool IsArchetype(IPublishedProperty property)
        {
            return property.DataValue.ToString().Contains("fieldset");
        }

        /// <summary>
        /// Method retrieves all virtual tabs from content 
        /// </summary>
        /// <param name="content">Content node</param>
        /// <returns>Virtual Tabs of a content node</returns>
        private static IEnumerable<PropertyGroup> GetTabs(IPublishedContent content)
        {          
            var service = ApplicationContext.Current.Services.ContentService;           
            var contentType = service.GetById(content.Id);
            return contentType.PropertyGroups;
        }

        /// <summary>
        /// Method retrieves the property's type name
        /// </summary>
        /// <param name="content">Content node</param>
        /// <param name="property">Content property</param>
        /// <returns>property type as string</returns>
        private static string GetPropertyName(IPublishedContent content, IPublishedProperty property)
        {
            var name = umbraco.library.GetPropertyTypeName(content.DocumentTypeAlias, property.PropertyTypeAlias);            
            return ToJsonPropName(name);
        }

        /// <summary>
        /// converts property name to Json conform property
        /// </summary>
        /// <param name="name">Property name</param>
        /// <returns></returns>
        private static string ToJsonPropName(string name)
        {
            name = name.Replace(" ", "");
            return Char.ToLowerInvariant(name[0]) + name.Substring(1);
        }
        
        /// <summary>
        /// The method retrieves property with filtering the properties
        /// </summary>
        /// <param name="content">Content node</param>
        /// <param name="filters">Property Filter list</param>
        /// <returns></returns>
        public static Dictionary<string, object> Map(IPublishedContent content, List<KartelMapperConfigModel> filters)
        {
            var filter = filters.FirstOrDefault(x => x.Name.ToLower().Equals(content.DocumentTypeAlias.ToLower()));
            if (filter == null)
            {
                return new Dictionary<string, object>();
            }
           
            var list = new Dictionary<string, object>();
            foreach (var property in content.Properties)
            {
                if (IsFiltered(content, filter, property))
                {
                    continue;
                }

                if (IsArchetype(property))
                {
                    var model = content.GetPropertyValue<ArchetypeModel>(property.PropertyTypeAlias);
                    list.Add(GetPropertyName(content, property), MapDictionary(model));
                }
                else
                {
                    list.Add(GetPropertyName(content, property), property.DataValue);
                }
            }

            return list;
        }

        private static List<KartelMapperConfigModel> GetFilters()
        {
            var basePath = System.AppDomain.CurrentDomain.BaseDirectory;
            using (StreamReader r = new StreamReader($@"{basePath}config/KartelMapperConfig.config"))
            {
                return JsonConvert.DeserializeObject<List<KartelMapperConfigModel>>(r.ReadToEnd());
            }
        }

        /// <summary>
        /// Retrieves Groups from a json config file
        /// </summary>
        /// <returns>String array</returns>
        private static string[] GetGroups()
        {
            var basePath = System.AppDomain.CurrentDomain.BaseDirectory;
            using (StreamReader r = new StreamReader($@"{basePath}config/KartelGroupsConfig.config"))
            {
                return JsonConvert.DeserializeObject<string[]>(r.ReadToEnd());
            }
        }

        /// <summary>
        /// Checks whether a property is filtered
        /// </summary>
        /// <param name="content">Content node</param>
        /// <param name="filter">Filter</param>
        /// <param name="property">Property of a content</param>
        /// <returns>true if the property in the filtered list, false otherwise</returns>
        private static bool IsFiltered(IPublishedContent content, KartelMapperConfigModel filter, IPublishedProperty property)
        {
            return !filter.Filters.Exists(x => x.Equals(GetPropertyName(content, property)));
        }

        /// <summary>
        /// Gets all properties from a Archetype Model
        /// </summary>
        /// <param name="model">Archetype Model</param>
        /// <returns>List of dictionaries</returns>
        internal static List<object> MapDictionary(ArchetypeModel model)
        {
            var list = new List<object>();
            foreach (var fieldset in model.Fieldsets)
            {               
               list.Add(Map(fieldset));
            }
                        
            return list;
        }

        /// <summary>
        /// Gets all properties from a Archetype Fieldset
        /// </summary>
        /// <param name="fieldSet">Archetype Fieldset</param>
        /// <returns>A Dictionary of fieldset properties</returns>
        private static Dictionary<string, object> Map(ArchetypeFieldsetModel fieldSet)
        {                       
           var propertyList = new Dictionary<string, object>();
           propertyList.Add("type", fieldSet.Alias);
           propertyList.Add("id", Guid.NewGuid());

            foreach (var property in fieldSet.Properties)
           {
               Map(property, propertyList);
           }
           
           return propertyList;
        }

        /// <summary>
        /// Adding Archetype properties to the dictionary
        /// </summary>
        /// <param name="property">Archetype Property</param>
        /// <param name="dict">Dictionary of archetype properties</param>
        private static void Map(ArchetypePropertyModel property, Dictionary<string, object> dict)
        {
            if (property.PropertyEditorAlias != null && property.PropertyEditorAlias.Equals("Imulus.Archetype"))
            {
                dict.Add(property.Alias, MapDictionary(property.GetValue<ArchetypeModel>()));
                return;
            }

            if (property.PropertyEditorAlias != null && (property.PropertyEditorAlias.Equals("Umbraco.MediaPicker2") || property.PropertyEditorAlias.Equals("Umbraco.ContentPicker2")))
            {
                var helper = new UmbracoHelper(UmbracoContext.Current);
                var urls = property.Value.ToString().Split(',');
                var typedContentList = new ArrayList();
                foreach (var url in urls)
                {
                    var udi = Udi.Parse(url);
                    if (property.PropertyEditorAlias.Equals("Umbraco.MediaPicker2"))
                    {
                        typedContentList.Add(helper.TypedMedia(udi).Url);
                    }
                    else
                    {
                        typedContentList.Add(helper.TypedContent(udi).Url);
                    }
                }
                dict.Add(property.Alias, typedContentList.ToArray());
                return;
            }
            
            if (property.PropertyEditorAlias != null && property.PropertyEditorAlias.Equals("Umbraco.CheckBoxList"))
            {                
                dict.Add(property.Alias, property.Value.ToString().Split(',').ToArray());
                return;
            }

            dict.Add(property.Alias, property.Value.ToString());
        }

        /// <summary>
        /// Method retrieves Children info from a contnet node 
        /// </summary>
        /// <param name="content">Content node</param>
        /// <returns>Short info of childrens as alist of dictionaries</returns>
        private static List<Dictionary<string, object>> GetChildrenList(IEnumerable<IPublishedContent> children, int numberOfNestedLevels=0)
        {
            var filters = GetFilters();
            var childrenList = new List<Dictionary<string, object>>();
            foreach (var publishedContent in children)
            {

                if (numberOfNestedLevels > 1)
                {
                    childrenList.Add(new Dictionary<string, object>()
                    {
                        {"pageName", publishedContent.Name},
                        {"pageType", publishedContent.DocumentTypeAlias},
                        {"pageId", publishedContent.Id},
                        {"pageUrl", publishedContent.Url},
                        {"pageContent", Map(publishedContent, filters)},
                        {"children", GetChildrenList(publishedContent.Children)},
                        {"BreadCrumbs", publishedContent.AncestorsOrSelf().Select(s => new { Name = s.Name, Url = s.Url }).ToList().SkipLast()}
                });
                }
                else
                {
                    childrenList.Add(new Dictionary<string, object>()
                    {
                        { "pageName", publishedContent.Name },
                        { "pageType", publishedContent.DocumentTypeAlias},
                        { "pageId", publishedContent.Id},
                        { "pageUrl", publishedContent.Url },
                        { "pageContent", Map(publishedContent, filters) },
                        {"BreadCrumbs", publishedContent.AncestorsOrSelf().Select(s => new { Name = s.Name, Url = s.Url }).ToList().SkipLast()}
                    });
                }                
            }
            return childrenList;
        }
    }
}